# Project Finite-Automata-Toolbox

[![c_plus_plus](https://img.shields.io/badge/Language-C%2B%2B-blue)](https://www.cplusplus.com/)
[![qt5](https://img.shields.io/badge/Framework-Qt5-green)](https://doc.qt.io/qt-5/) <br>

The goal of our project is to create an easy to use enviroment with a basic set of operations on finite state automata.

Implemented functionalities:
- Construction of a finite state automata out of a regular expression
- Determinization and minimization of automata
- Union, intersection, difference and complement of automata
- Conversion of automata into regular expressions
- Test if the automaton recognizes a given string

The interface contains a "work area" where graph representation of automata is drawn and the above mentioned functionalities can be used.

Currently tested and working only on `Linux`.

## Prerequisites:
- Qt Creator
- Flex
- Bison
- Graphviz

### Setup:

Download and install the Qt library and Qt Creator from [here](https://www.qt.io/). <br>
Run the script included in the folder of the project to install Flex, Bison, Graphviz and Catch2 (for testing).
```bash
   $ ./InstallDependenciesScript.sh
```
Open `finite_automata_toolbox.pro` from the folder of the project in Qt creator.
Run the program.

## Demo video:
[Link to demo video](https://drive.google.com/file/d/1Ctng2DnxELJ-Jg84zGnVZd8aOu6sPrbK/view)

## Developers:

- [Minja Popović, 39/2018](https://gitlab.com/Prophethor)
- [Aleksandar Stefanović, 227/2018](https://gitlab.com/MistFuror)
- [Aleksa Papić, 492/2018](https://gitlab.com/pap1rana)
- [Martin Župčić, 115/2018](https://gitlab.com/tinmargitlab)
- [Boško Delić, 256/2018](https://gitlab.com/BoskoDelic)
