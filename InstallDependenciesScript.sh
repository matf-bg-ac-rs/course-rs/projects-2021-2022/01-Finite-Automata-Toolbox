#flex
sudo apt install flex

#bison
sudo apt install bison

# graphviz
sudo apt install graphviz-dev

# catch2 v2
git clone -b v2.x https://github.com/catchorg/Catch2.git ./3rd/Catch2
